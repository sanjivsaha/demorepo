package com.capgemini.service;

import com.capgemini.exceptions.InsufficientBalanceException;
import com.capgemini.exceptions.InsufficientInitialAmountException;
import com.capgemini.exceptions.InvalidAccountNumberException;
import com.capgemini.model.Account;
import com.capgemini.repository.AccountRepository;

public class AccountServiceImpl implements AccountService {
	
	/* (non-Javadoc)
	 * @see com.capgemini.service.AccountService#createAccount(int, int)
	 */
	
	AccountRepository accountRepository;
	
	
	public AccountServiceImpl(AccountRepository accountRepository) {
		super();
		this.accountRepository = accountRepository;
	}


	@Override
	public Account createAccount(int accountNumber,int amount) throws InsufficientInitialAmountException
	{
		int a,b,c;
		if(amount<500)
		{
			throw new InsufficientInitialAmountException();
		}
		Account account = new Account();
		account.setAccountNumber(accountNumber);
		
		account.setAmount(amount);
		 
		if(accountRepository.save(account))
		{
			return account;
		}
	     
		return null;
		
	}
	
	@Override
	public Account depositAmount(Integer accountNumber, int amount) throws InvalidAccountNumberException
	{
		int a,b,c;
		Account account;
		account = new Account();
		if(accountNumber==0)
		{
			throw new InvalidAccountNumberException();
		}
		boolean flag; 
		flag=accountRepository.searchAccount(accountNumber);
		if(flag)
		{
		    account.setAccountNumber(accountNumber);
			account.setAmount(amount);
		}

		Integer accNum=account.getAccountNumber();
		if(accNum!=null)
		{
			return account;
		}
		else
		{
			return null;
		}
	}
	
	public Account withdrawAmount(Integer accountNumber, int amount) throws InvalidAccountNumberException,InsufficientBalanceException
	{
		Integer balance=1000;
		Account account=new Account();
		account.setAccountNumber(accountNumber);
		account.setAmount(amount);
		if(accountNumber==0)
		{
			throw new InvalidAccountNumberException();
		}
		if(amount > balance)
		{
			throw new InsufficientBalanceException();
		}
		else
		{
			return account;
		}
	}

}
