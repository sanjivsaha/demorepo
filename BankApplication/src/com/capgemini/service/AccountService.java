package com.capgemini.service;

import com.capgemini.exceptions.InsufficientBalanceException;
import com.capgemini.exceptions.InsufficientInitialAmountException;
import com.capgemini.exceptions.InvalidAccountNumberException;
import com.capgemini.model.Account;

public interface AccountService {

	Account createAccount(int accountNumber, int amount) throws InsufficientInitialAmountException;
	Account depositAmount(Integer accountNumber, int amount) throws InvalidAccountNumberException;
	Account withdrawAmount(Integer accountNumber, int amount) throws InvalidAccountNumberException,InsufficientBalanceException;
}